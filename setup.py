#!/usr/bin/env python

# Copyright (C) 2012, 2013 David Villa Alises
#
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA


from setuptools import setup, find_packages

setup(name         = 'commodity',
      version      = '0.20140102',
      description  = 'General purpose python utilities library',
      author       = 'David Villa Alises',
      author_email = 'David.Villa@gmail.com',
      url          = 'https://bitbucket.org/arco_group/python-commodity',
      license      = 'GPL v2 or later',
      packages     = find_packages(),
      provides     = ['commodity'],
      )
