#!/usr/bin/make -f
# -*- mode:makefile -*-

URL_AUTH=svn+ssh://${ALIOTH_USER}@svn.debian.org/svn/python-modules/packages/commodity/trunk
URL_ANON=svn://svn.debian.org/svn/python-modules/packages/commodity/trunk

debian:
	if [ ! -z "$${ALIOTH_USER}" ]; then \
	    svn co ${URL_AUTH} -N; \
	else \
	    svn co ${URL_ANON} -N; \
	fi

	mv trunk/.svn .
	rmdir trunk
	svn up debian

wiki:
	hg clone https://bitbucket.org/arco_group/python-commodity/wiki

clean:
	$(RM) -r doc/_build build dist commodity.egg-info

vclean:
	$(RM) -r .svn debian MANIFEST
