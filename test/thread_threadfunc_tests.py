# -*- coding:utf-8; tab-width:4; mode:python -*-

import threading
import time
from unittest import TestCase

from commodity.thread_ import ThreadFunc


class MayBeStopped(object):
    def __init__(self):
        self.end = threading.Event()
        self.value = 0

    def stop(self):
        self.end.set()

    def __call__(self):
        while not self.end.isSet():
            time.sleep(0.1)
            self.value += 1


class TestThreadFunc(TestCase):
    def test_stop_threadedFunc(self):
        func = MayBeStopped()
        ThreadFunc(func)
        time.sleep(.5)
        func.stop()

        self.assertTrue(func.value > 0)
