# -*- coding:utf-8; tab-width:4; mode:python -*-

from unittest import TestCase
import collections
import argparse
import json

from configobj import ConfigObj
from validate import Validator

from commodity.pattern import Bunch, NestedBunch, MetaBunch


class BunchTests(TestCase):
    def setUp(self):
        self.bunch = Bunch()
        self.bunch['initial'] = True

    def test_keyword_is_attribute(self):
        sut = Bunch(foo=1)
        sut.foo

    def test_init_from_dict(self):
        d = dict(foo=1)
        sut = Bunch(d)
        sut.foo

    def test_add_key_is_attr(self):
        self.assert_(self.bunch.initial)
        self.assertEquals(self.bunch.keys(), ['initial'])
        self.assertEquals(self.bunch.values(), [True])

    def test_new_attribute_is_key(self):
        self.bunch.foo = True
        self.assert_(self.bunch['foo'])

    def test_del_key_removes_attribute(self):
        del self.bunch['initial']
        with self.assertRaises(AttributeError):
            self.bunch.initial

    def test_del_attribute_removes_key(self):
        del self.bunch.initial
        with self.assertRaises(KeyError):
            self.bunch['initial']

    def test_copy_creates_bunch(self):
        sut = self.bunch.copy()
        self.assert_(isinstance(sut, Bunch))
        sut.initial

    def test_repr(self):
        # given
        self.bunch.foo = 2
        self.bunch.bar = 'ab'

        # when
        text = repr(self.bunch)

        # then
        self.assertEquals(text,
                          "Bunch('initial': True, 'bar': 'ab', 'foo': 2)")


class BunchDerived(TestCase):

    class Derived(Bunch):
        static = 100

    def setUp(self):
        self.bunch = self.Derived()

    def test_add_attr(self):
        self.bunch.foo = 1
        self.assertEquals(self.bunch['foo'], 1)
        self.assertEquals(self.bunch.keys(), ['foo'])

#    def test_class_attr(self):
#        self.assertEquals(self.bunch['static'], 100)


class NestedBunchTests(TestCase):
    def setUp(self):
        self.bunch = NestedBunch()

    def test_empty_has_nothing(self):
        self.assertEqual(self.bunch.keys(), [])

    def test_missing_key(self):
        with self.assertRaises(KeyError):
            self.bunch['missing']

    def test_missing_attribute(self):
        with self.assertRaises(AttributeError):
            self.bunch.missing

    def test_inherited_keys(self):
        # given
        self.bunch.foo = 1

        # when
        sut = self.bunch.new_layer()
        sut.bar = 2

        # then
        sut.foo
        self.assertItemsEqual(sut.keys(), ['foo', 'bar'])

    def test_values(self):
        # given
        self.bunch.foo = 1

        # when
        sut = self.bunch.new_layer()
        sut.bar = 2

        # then
        self.assertItemsEqual(sut.values(), [1, 2])

    def test_items(self):
        self.bunch.foo = 1

        sut = self.bunch.new_layer()
        sut.bar = 2

        self.assertItemsEqual(sut.items(), [('foo', 1), ('bar', 2)])

    def test_copy_is_a_layerbunch(self):
        sut = self.bunch.copy()
        self.assert_(isinstance(sut, NestedBunch))

    def test_override_dont_overwrite__by_key(self):
        self.bunch['foo'] = 'foo'

        sut = self.bunch.new_layer()
        sut['foo'] = 'foofoo'

        self.assertEqual(self.bunch['foo'], 'foo')
        self.assertEqual(sut['foo'], 'foofoo')

    def test_override_dont_overwrite___by_attribute(self):
        self.bunch.foo = 'foo'

        sut = self.bunch.new_layer()
        sut.foo = 'foofoo'

        self.assertEqual(self.bunch.foo, 'foo')
        self.assertEqual(sut.foo, 'foofoo')

    def test_fallback_by_key(self):
        self.bunch['foo'] = 'foo'

        sut = self.bunch.new_layer()
        self.assertEqual(sut['foo'], 'foo')

    def test_fallback_by_attribute(self):
        self.bunch.foo = 'foo'

        sut = self.bunch.new_layer()
        self.assertEqual(sut.foo, 'foo')

    def test_2_level_override__by_key(self):
        self.bunch['foo'] = 'foo'

        sut = self.bunch.new_layer().new_layer()
        sut['foo'] = 'foofoo'

        self.assertEqual(self.bunch['foo'], 'foo')
        self.assertEqual(sut['foo'], 'foofoo')

    def test_2_level_override__by_attribute(self):
        self.bunch.foo = 'foo'

        sut = self.bunch.new_layer().new_layer()
        sut.foo = 'foofoo'

        self.assertEqual(self.bunch.foo, 'foo')
        self.assertEqual(sut.foo, 'foofoo')

    def test_2_level_fallback__by_key(self):
        self.bunch['foo'] = 'foo'

        sut = self.bunch.new_layer().new_layer()
        self.assertEqual(sut['foo'], 'foo')

    def test_2_level_fallback__by_attribute(self):
        self.bunch.foo = 'foo'

        sut = self.bunch.new_layer().new_layer()
        self.assertEqual(sut.foo, 'foo')


class MetaBunchTests(TestCase):
    def test_create(self):
        sut = MetaBunch()
        self.assert_(isinstance(sut, collections.Mapping))
        self.assert_(isinstance(sut, collections.MutableMapping))

    def test_create_with_dict(self):
        MetaBunch(dict())

    def test_create_with_non_dict_is_error(self):
        with self.assertRaises(TypeError):
            MetaBunch([])

    def test_keys_must_be_strings(self):
        try:
            sut = MetaBunch()
            sut[1]
            self.fail()
        except TypeError:
            pass

    def test_missing_key(self):
        try:
            sut = MetaBunch(dict())
            sut['missing']
            self.fail()
        except KeyError:
            pass

    def test_missing_leaf_key(self):
        try:
            sut = MetaBunch(dict(aaa=dict()))
            sut['aaa.missing']
            self.fail()
        except KeyError:
            pass

    def test_missing_intermediate_key(self):
        try:
            sut = MetaBunch()
            sut['missing.other']
            self.fail()
        except KeyError:
            pass

    def test_get_key_1_level(self):
        dct = dict(a=1)
        sut = MetaBunch(dct)
        self.assertEquals(sut['a'], 1)

    def test_get_key_2_level(self):
        sut = MetaBunch(dict(aaa=dict(bbb=1)))
        self.assertEquals(sut['aaa.bbb'], 1)

    def test_fail_intermediate_is_not_a_dict(self):
        try:
            sut = MetaBunch(dict(aaa=1))
            sut['aaa.bbb']
            self.fail()
        except TypeError:
            pass

    def test_set_key_1_level(self):
        dct = dict()
        sut = MetaBunch(dct)
        sut['a'] = 1
        self.assertEquals(sut['a'], 1)
        self.assertEquals(dct['a'], 1)

    def test_set_key_2_level(self):
        dct = dict()
        sut = MetaBunch(dct)
        sut['aaa.bbb'] = 1
        self.assertEquals(sut['aaa.bbb'], 1)
        self.assertEquals(dct['aaa'], dict(bbb = 1))

    def test_keys(self):
        sut = MetaBunch(dict(a=1))
        self.assertEquals(sut.keys(), ['a'])

    def test_repr(self):
        sut = MetaBunch(dict(a=1))
        self.assertEquals(repr(sut), "{'a': 1}")

    def test_update(self):
        sut = MetaBunch(dict(a=1))
        other = {}
        other.update(sut)
        self.assertEquals(other.keys(), ['a'])

    def test_haskey_1_level(self):
        sut = MetaBunch(dict(a=1))
        self.assert_('a' in sut)

    def test_haskey_2_level(self):
        sut = MetaBunch(dict())
        sut['aaa.bbb'] = 1
        self.assert_('aaa.bbb' in sut)

    def test_haskey_2_level_intermediate_is_not_dict(self):
        sut = MetaBunch(dict(aaa=1))
        self.assert_('aaa.bbb' not in sut)

    def test_del_key_1_level(self):
        sut = MetaBunch(dict(aaa=1))
        del sut['aaa']

    def test_del_key_2_level(self):
        dct = dict(aaa=dict(bbb=1))
        sut = MetaBunch(dct)
        del sut['aaa.bbb']

        self.assert_('aaa.bbb' not in sut)
        self.assert_('aaa' in sut)

    def test_get_attr_1_level(self):
        sut = MetaBunch()
        sut['aaa'] = 1
        self.assertEquals(sut.aaa, 1)

    def test_get_attr_2_level(self):
        sut = MetaBunch()
        sut['aaa.bbb'] = 1
        self.assertEquals(sut.aaa.bbb, 1)

    def test_get_attr_3_level(self):
        sut = MetaBunch()
        sut['aaa.bbb.ccc'] = 1
        self.assertEquals(sut.aaa.bbb.ccc, 1)

    def test_set_attr_1_level(self):
        sut = MetaBunch()
        sut.aaa = 1
        self.assertEquals(sut['aaa'], 1)

    def test_set_attr_2_level(self):
        sut = MetaBunch()
        sut.aaa = {}
        sut.aaa.bbb = 1
        self.assertEquals(sut['aaa.bbb'], 1)

    def test_intermediate_is_MetaBunch_by_key(self):
        sut = MetaBunch()
        sut.aaa = dict(bbb=1)
        self.assert_(isinstance(sut['aaa'], MetaBunch))

    def test_intermediate_is_MetaBunch_by_attr(self):
        sut = MetaBunch()
        sut.aaa = dict(bbb=1)
        self.assert_(isinstance(sut.aaa, MetaBunch))


class ArgparseParser_with_MetaBunch(TestCase):
    def test_dest_1_level(self):
        sut = MetaBunch()

        parser = argparse.ArgumentParser()
        parser.add_argument('--aaa')

        config = parser.parse_args("--aaa foo".split(), namespace=sut)

        self.assertEquals(config.aaa, 'foo')

    def test_dest_2_level(self):
        sut = MetaBunch(dict(aaa={}))

        parser = argparse.ArgumentParser()
        parser.add_argument('--bbb', dest='aaa.bbb')

        config = parser.parse_args("--bbb foo".split(), namespace=sut)

        self.assertEquals(config.aaa.bbb, 'foo')

CONFIGOBJ_SPEC = '''\
    verbose = boolean
    ratio = float
    color = options(boolean, always, auto, never, default=auto)
    [status]
    subrepos = boolean(default=false)
    [tag]
    message = string'''.splitlines()

CONFIGOBJ_FILE = '''
    verbose = True
    ratio = 1.0
    [status]
    subrepos = True
    [tag]
    message = foo'''.splitlines()


class MetaBunched_ConfigObj(TestCase):
    def test_2_level(self):
        config = ConfigObj(CONFIGOBJ_FILE)
        sut = MetaBunch(config)
        self.assertEquals(sut.ratio, '1.0')
        self.assertEquals(sut.status.subrepos, 'True')
        self.assertEquals(sut.tag.message, 'foo')

    def test_2_level_with_spec(self):
        config = ConfigObj(CONFIGOBJ_FILE, configspec=CONFIGOBJ_SPEC)
        self.assert_(config.validate(Validator()))
        sut = MetaBunch(config)
        self.assertEquals(sut.ratio, 1.0)
        self.assertEquals(sut.status.subrepos, True)


class ArgumentParser_with_ConfigObj(TestCase):
    def test_default(self):
        d = ConfigObj()

        parser = argparse.ArgumentParser()
        parser.add_argument('--foo')

        config = parser.parse_args("--foo bar".split(), namespace=d)

        self.assertEquals(config.foo, 'bar')


class ArgumentParser_with_MetaBunched_ConfigObj(TestCase):
    def test_dest_1_level(self):
        sut = MetaBunch(ConfigObj())

        parser = argparse.ArgumentParser()
        parser.add_argument('--foo')

        parser.parse_args("--foo bar".split(), namespace=sut)

        self.assertEquals(sut.foo, 'bar')

    def test_dest_2_level(self):
        sut = MetaBunch(ConfigObj())

        parser = argparse.ArgumentParser()
        parser.add_argument('--foo', dest='sec.foo')

        parser.parse_args("--foo bar".split(), namespace=sut)

        self.assertEquals(sut.sec.foo, 'bar')

    def test_default_from_config_spec(self):
        configobj = ConfigObj(configspec=CONFIGOBJ_SPEC)
        configobj.validate(Validator())
        sut = MetaBunch(configobj)

        parser = argparse.ArgumentParser()
        parser.add_argument('--subrepos', dest="status.subrepos")

        self.assertEquals(sut.status.subrepos, False)

    def test_override_default_with_args(self):
        configobj = ConfigObj(CONFIGOBJ_FILE, configspec=CONFIGOBJ_SPEC)
        self.assert_(configobj.validate(Validator()))
        sut = MetaBunch(configobj)

        parser = argparse.ArgumentParser()
        parser.add_argument('--ratio', dest="ratio", type=float)
        parser.parse_args("--ratio 2".split(), namespace=sut)

        self.assertEquals(sut.ratio, 2.0)


class Json_with_MetaBunch(TestCase):
    def setUp(self):
        self.json_file = '''\
[
  {
    "a": "A",
    "b": [
      2,
      4
    ],
    "bb": {"bbb": 1.1},
    "d": 3.0
  }
]'''

    def test_2_level(self):
        data = json.loads(self.json_file)

        sut = MetaBunch(data[0])
        self.assertEquals(sut.a, "A")
        self.assertEquals(sut.d, 3.0)
        self.assertEquals(sut.b[1], 4)
        self.assertEquals(sut.bb.bbb, 1.1)

    def test_dump(self):
        mb = MetaBunch({"a": "A"})
        data = [dict(mb)]
        print json.dumps(data)
