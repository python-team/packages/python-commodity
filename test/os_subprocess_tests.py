# -*- coding:utf-8; tab-width:4; mode:python -*-

import time
import io
from unittest import TestCase

from hamcrest import assert_that, is_, contains_string

import commodity.thread_ as thread_
import commodity.os_ as os_

from nose.plugins.attrib import attr


class ReaderThreadTests(TestCase):
    def test_reading(self):
        text = "sample"
        pipe = os_.Pipe()
        sink = io.BytesIO()

        pipe.write_end.write(text)
        reader = thread_.ReaderThread(pipe.read_end, sink)
        time.sleep(0.1)

        thread_.start_new_thread(pipe.write_end.close)
        reader.join(1)

        self.assertEquals(sink.getvalue(), text)


class SubProcessTests(TestCase):
    def test_command(self):
        out = io.BytesIO()
        cmd = os_.SubProcess('yes', stdout=out)
        time.sleep(0.5)
        cmd.terminate()
        cmd.wait()
        self.assert_(out.getvalue().count('y') > 10)

    def test_missing_command(self):
        cmd = os_.SubProcess('wrong', shell=True)
        cmd.wait()
        assert_that(cmd.returncode, is_(127))

    def test_redirection(self):
        cmd = os_.SubProcess('echo hi1 > /tmp/kk', shell=True)
        cmd.wait()
        assert_that(cmd.returncode, is_(0))
        assert_that(file('/tmp/kk').read(), contains_string('hi1'))

    def test_pipe(self):
        out = io.BytesIO()
        cmd = os_.SubProcess('echo hi2 | sort', stdout=out, shell=True)
        cmd.wait()
        assert_that(cmd.returncode, is_(0))
        assert_that(out.getvalue(), contains_string('hi2'))

    def test_env(self):
        value = 'my-env-var'
        out = io.BytesIO()
        cmd = os_.SubProcess('echo $MYVAR', stdout=out, shell=True,
                             env={'MYVAR': value})
        cmd.wait()
        assert_that(out.getvalue(), contains_string(value))


class Test_check_output(TestCase):
    def test_check_argument_as_list(self):
        out = os_.check_output('echo hi everyone')
        self.assertEquals(out.strip(), 'hi everyone')

    def test_check_output_ok(self):
        out = os_.check_output('echo hi')
        self.assertEquals(out.strip(), 'hi')

    def test_check_output_fail(self):
        with self.assertRaises(os_.CalledProcessError) as cm:
            os_.check_output('false')

        self.assertEquals(cm.exception.cmd, 'false')
        self.assertEquals(cm.exception.returncode, 1)

    @attr('test')
    def test_check_output_fail_with_stderr(self):
        cmd = 'test/util/print_err_and_fail.py'

        with self.assertRaises(os_.CalledProcessError) as cm:
            os_.check_output(cmd)

        self.assertEquals(cm.exception.cmd, cmd)
        self.assertEquals(cm.exception.returncode, 1)
        self.assertEquals(cm.exception.output, 'wrong->out')
        self.assertEquals(cm.exception.err, 'wrong->err')
