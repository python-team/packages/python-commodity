# -*- coding:utf-8; tab-width:4; mode:python -*-

import unittest
import functools

from commodity.os_ import SubProcess, wait_or_raise
from commodity.net import is_port_open


class wait_or_raise_tests(unittest.TestCase):
    def test_wait_server_ready__ok(self):
        assert not is_port_open(2000)
        server = SubProcess('ncat -l 2000')
        wait_or_raise(functools.partial(is_port_open, 2000),
                      delta=0.5)
        server.terminate()

    def test_wait_server_ready__fail(self):
        assert not is_port_open(2000)
        assert not is_port_open(5000)
        server = SubProcess('ncat -l 2000')

        with self.assertRaises(AssertionError):
            wait_or_raise(functools.partial(is_port_open, 5000),
                          delta=0.5, timeout=1)

        server.terminate()

#    def test_wait_server_become_ready(self):
#        assert_that(localhost, is_not(listen_port(2000)))
#        server = SubProcess('ncat -l 2000')
#        wait_that(localhost, listen_port(2000))
#        server.terminate()
