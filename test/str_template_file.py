# -*- mode:python; coding:utf-8; tab-width:4 -*-

import tempfile

from unittest import TestCase
from hamcrest import assert_that, is_

from commodity.str_ import TemplateFile


class test_resolve_path(TestCase):
    def test_render(self):
        t = TemplateFile('$who likes $what')
        assert_that(t.render(dict(who='kate', what='rice')),
                    is_('kate likes rice'))

    def test_render_to_file(self):
        fd, filename = tempfile.mkstemp()

        t = TemplateFile('$who likes $what')
        t.render_to_file(dict(who='kate', what='rice'), filename)

        assert_that(file(filename).read(),
                    is_('kate likes rice'))

    def test_render_to_tempfile(self):
        t = TemplateFile('$who likes $what')
        filename = t.render_to_tempfile(dict(who='kate', what='rice'))

        assert_that(file(filename).read(),
                    is_('kate likes rice'))

    def test_from_file(self):
        fd, filename = tempfile.mkstemp()
        with file(filename, 'wt') as fd:
            fd.write('$who likes $what')

        t = TemplateFile.from_file(filename)
        assert_that(t.render(dict(who='kate', what='rice')),
                    is_('kate likes rice'))
