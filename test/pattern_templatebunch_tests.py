# -*- coding:utf-8; tab-width:4; mode:python -*-

import sys
sys.path.insert(0, '.')

from unittest import TestCase

from commodity.pattern import TemplateBunch


class TemplateBunchTests(TestCase):
    def test_read_str_with_str_var(self):
        t = TemplateBunch()
        t.name = 'Bob'
        t.greeting = 'Hi $name!'

        self.assertEquals(t.greeting, 'Hi Bob!')

    def test_read_unicode_with_str_var(self):
        t = TemplateBunch()
        t.name = 'Bob'
        t.greeting = u'Hi $name!'

        self.assertEquals(t.greeting, u'Hi Bob!')

    def test_read_str_with_int_var(self):
        t = TemplateBunch()
        t.name = 10
        t.greeting = 'Hi $name!'

        self.assertEquals(t.greeting, 'Hi 10!')

    def test_read_int(self):
        t = TemplateBunch()
        t.greeting = 20

        self.assertEquals(t.greeting, 20)

    def test_clear(self):
        t = TemplateBunch()
        t.name = 'Alice'
        t.clear()

        with self.assertRaises(AttributeError):
            t.name

    def test_nested_interpolation(self):
        t = TemplateBunch()
        t.name = 'Bob'
        t.person = "$name's wife"
        t.greeting = 'Hi $person!'

        self.assertEquals(t.greeting, "Hi Bob's wife!")
        self.assertEquals(t.person, "Bob's wife")

    def test_recurse_interpolation(self):
        t = TemplateBunch()
        t.name = "Bob's $thing"
        t.thing = "Car of $name"

        with self.assertRaises(RuntimeError):
            t.name
